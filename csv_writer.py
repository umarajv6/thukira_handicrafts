#!/usr/bin/python
from tempfile import NamedTemporaryFile
import shutil
import csv

with open('export.csv','rb') as fin:
    dreader = csv.DictReader(fin)
    with open('export_copy.csv','w') as fout:
        dwriter = csv.DictWriter(fout,delimiter=',', fieldnames=dreader.fieldnames,extrasaction='ignore')
        #dwriter.writerow(dict((fn,fn) for fn in fieldnames))
        dwriter.writeheader()
        for row in dreader:
            print row['sku']
            print ("Items dimensions in inches - height:%s ,width:%s and length:%s "
                    %(row['height'],row['width'],row['length']))
            row['post_content'] = (row['post_content'] + "Dimensions height x width x length:" +
                   str(round(float(row['height'] or 0))) +
                    "x" + str(round(float(row['width'] or 0))) +
                    "x" + str(round(float(row['length'] or 0))))
            dwriter.writerow(row )



"""
class ThukiraCSV(object):
    def __init__(self,filename):
        self.filecp = 'modified_file.csv'
        shutil.copyfile(filename,self.filecp)
        self.tempfile = NamedTemporaryFile(delete=False)
    
    def modify_post_content(self):
        with open(self.filecp, 'rb') as csvFile,self.tempfile:
            reader = csv.reader(csvFile, delimiter=',', quotechar='"')
            writer = csv.writer(self.tempfile, delimiter=',', quotechar='"')
            _dictreader = csv.DictReader(csvFile)
            for row in reader:
                row[1] = row[1].title()
            for dictrow in _dictreader:
                if dictrow['post_content']:
                    dictrow['post_content'] = dictrow['post_content'] + "\n" + \
                        "Dimensions height x width x length:" + \
                        str(round(int(dictrow['height'])*2.54)) + \
                        "x" + str(round(int(dictrow['width'])*2.54)) + \
                        "x" + str(round(int(dictrow['length'])*2.54)) 
                writer.writerow(row)
        shutil.move(self.tempfile.name, self.filecp)

if __name__ == "__main__":
    tcsv = ThukiraCSV('export.csv')
    tcsv.modify_post_content()

"""