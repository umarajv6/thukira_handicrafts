#!/usr/bin/python

import csv
with open('new_csv_file.csv','rb') as thukira:
    reader = csv.DictReader(thukira)
    for row in reader:
        if row['sku'] == "RPWMRE":
            print ("Items dimensions in inches - height:%s ,width:%s and length:%s "
                    %(row['height'],row['width'],row['length']))
            print (row['post_content'] + "\n" + "Dimensions height x width x length:" +
                    str(round(int(row['height'])*2.54)) + 
                    "x" + str(round(int(((row['width']) or 0)*2.54))) +
                   "x" + str(round(int(row['length'])*2.54)))
#            print row['post_content']
